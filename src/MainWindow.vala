/*
    Copyright (c) 2015 - 2025, Vinari Software
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

namespace UI {
	[GtkTemplate (ui="/org/vinarisoftware/permessi/UI/MainWindow.ui")]
	public class MainWindow : Gtk.ApplicationWindow {
		[GtkChild] private unowned Gtk.Box mainBox;
		[GtkChild] private unowned Gtk.HeaderBar headerBar;

		private Gtk.ToggleButton fileToggleButton;
		private Gtk.ToggleButton folderToggleButton;

		private Gtk.Label selectLocationLabel;
		private Gtk.Entry selectLocationEntry;
		private Gtk.Button selectLocationButton;

		private Gtk.Label individualPermissionsLabel;
		private Gtk.Label ownerPermissionLabel;
		private Gtk.Label groupPermissionLabel;
		private Gtk.Label otherPermissionLabel;

		private Gtk.Entry ownerPermissionEntry;
		private Gtk.Entry groupPermissionEntry;
		private Gtk.Entry otherPermissionEntry;

		private Gtk.Label argumentsLabel;
		private Gtk.Entry argumentsEntry;

		private Gtk.Button applyButton;
		private GLib.Settings mySettings;

		private App.Widgets.MessageDialog msgDialog;
		private Gtk.FileChooserNative fileChooser;

		public MainWindow (Gtk.Application app) {
			Object (application: app);
		}
		construct{
			this.mainBox.set_orientation(Gtk.Orientation.VERTICAL);
			this.set_title("Permessi - Vinari Software");
			this.set_resizable(false);

			mySettings=new GLib.Settings("org.vinarisoftware.permessi");

			Gtk.Box radioButtonsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 15);
			Gtk.Box selectLocationBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);

			Gtk.Box ownerBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box groupBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box otherBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box individualPermissionsBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 7);
			Gtk.Box individualPermissionsSubBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 12);
			Gtk.Box argumentsBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 12);

			folderToggleButton=new Gtk.ToggleButton.with_label(_("Directory"));
			fileToggleButton=new Gtk.ToggleButton.with_label(_("File"));

			if(mySettings.get_boolean("filefolder")==true){
				fileToggleButton.set_active(true);
			}else{
				folderToggleButton.set_active(true);
			}

			folderToggleButton.set_group(fileToggleButton);

			selectLocationLabel=new Gtk.Label(_("Select a file to alter its permissions:"));
			selectLocationEntry=new Gtk.Entry();
			selectLocationEntry.set_hexpand(true);

			Gtk.Box selectLocationButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Label selectLocationButtonLabel=new Gtk.Label(_("Browse"));
			Gtk.Image selectLocationButtonImage=new Gtk.Image.from_icon_name("folder-open-symbolic");

			selectLocationButtonBox.append(selectLocationButtonImage);
			selectLocationButtonBox.append(selectLocationButtonLabel);

			selectLocationButton=new Gtk.Button();
			selectLocationButton.set_child(selectLocationButtonBox);

			selectLocationBox.append(selectLocationLabel);
			selectLocationBox.append(selectLocationEntry);
			selectLocationBox.append(selectLocationButton);

			individualPermissionsLabel=new Gtk.Label(_("Set the individual permissions:"));

			ownerPermissionLabel=new Gtk.Label(_("Owner"));
			groupPermissionLabel=new Gtk.Label(_("Group"));
			otherPermissionLabel=new Gtk.Label(_("Others"));

			ownerPermissionEntry=new Gtk.Entry();
			ownerPermissionEntry.set_hexpand(true);
			ownerPermissionEntry.set_placeholder_text("7");
			ownerPermissionEntry.set_text(mySettings.get_int("owner").to_string());
			ownerPermissionEntry.set_input_purpose(Gtk.InputPurpose.DIGITS);

			groupPermissionEntry=new Gtk.Entry();
			groupPermissionEntry.set_hexpand(true);
			groupPermissionEntry.set_placeholder_text("4");
			groupPermissionEntry.set_text(mySettings.get_int("group").to_string());
			groupPermissionEntry.set_input_purpose(Gtk.InputPurpose.DIGITS);

			otherPermissionEntry=new Gtk.Entry();
			otherPermissionEntry.set_hexpand(true);
			otherPermissionEntry.set_placeholder_text("4");
			otherPermissionEntry.set_text(mySettings.get_int("others").to_string());
			otherPermissionEntry.set_input_purpose(Gtk.InputPurpose.DIGITS);

			ownerBox.append(ownerPermissionLabel);
			ownerBox.append(ownerPermissionEntry);

			groupBox.append(groupPermissionLabel);
			groupBox.append(groupPermissionEntry);

			otherBox.append(otherPermissionLabel);
			otherBox.append(otherPermissionEntry);

			argumentsLabel=new Gtk.Label(_("Additional arguments to pass to 'chmod':"));

			argumentsEntry=new Gtk.Entry();
			argumentsEntry.set_placeholder_text("--verbose --preserve-root");
			argumentsEntry.set_text(mySettings.get_string("options"));

			radioButtonsBox.set_halign(CENTER);

			Gtk.Box applyButtonBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
			Gtk.Label applyButtonLabel=new Gtk.Label(_("Apply"));
			Gtk.Image applyButtonImage=new Gtk.Image.from_icon_name("emblem-ok-symbolic");

			applyButton=new Gtk.Button();
			applyButtonBox.append(applyButtonImage);
			applyButtonBox.append(applyButtonLabel);

			applyButton.set_child(applyButtonBox);
			headerBar.pack_start(applyButton);

			argumentsBox.append(argumentsLabel);
			argumentsBox.append(argumentsEntry);

			individualPermissionsSubBox.append(ownerBox);
			individualPermissionsSubBox.append(groupBox);
			individualPermissionsSubBox.append(otherBox);

			individualPermissionsBox.append(individualPermissionsLabel);
			individualPermissionsBox.append(individualPermissionsSubBox);

			radioButtonsBox.append(folderToggleButton);
			radioButtonsBox.append(fileToggleButton);

			mainBox.append(radioButtonsBox);
			mainBox.append(selectLocationBox);
			mainBox.append(individualPermissionsBox);
			mainBox.append(argumentsBox);

			selectLocationButton.clicked.connect(selectLocationButtonAction);
			applyButton.clicked.connect(applyButtonAction);
			fileToggleButton.toggled.connect(fileToggleButtonAction);
			folderToggleButton.toggled.connect(folderToggleButtonAction);
			this.close_request.connect(beforeClose);
		}

		private bool beforeClose(){
			mySettings.set_int("owner", int.parse(ownerPermissionEntry.get_text()));
			mySettings.set_int("group", int.parse(groupPermissionEntry.get_text()));
			mySettings.set_int("others", int.parse(otherPermissionEntry.get_text()));

			mySettings.set_string("options", argumentsEntry.get_text());

			return false;				// If set to true, the window will never close
		}

		private void generateNotification(string notifID, string notifMessage){
			Gtk.Application? app=get_application();
			Notification notification = new Notification ("Permessi - Vinari Software");
			notification.set_priority(HIGH);
			notification.set_title(_("Permissions applied correctly!"));
			notification.set_body(notifMessage);
			app.send_notification (notifID, notification);
		}

		private void folderToggleButtonAction(){
			selectLocationLabel.set_text(_("Select a directory to alter its permissions:"));
			mySettings.set_boolean("filefolder", false);
		}

		private void fileToggleButtonAction(){
			selectLocationLabel.set_text(_("Select a file to alter its permissions:"));
			mySettings.set_boolean("filefolder", true);
		}

		private void fileChooserAction(int response){
			if(response==Gtk.ResponseType.ACCEPT){
				selectLocationEntry.set_text(fileChooser.get_file().get_path());
			}else{
				return;
			}
		}

		private void selectLocationButtonAction(){
			if(mySettings.get_boolean("filefolder")){				// If it's true, then the file selector should be for a file
				fileChooser=new Gtk.FileChooserNative(_("Select a file to alter its permissions..."), this, Gtk.FileChooserAction.OPEN, _("Choose file"), _("Cancel"));
			}else{													// If it's false, then the file selector should be for a folder
				fileChooser=new Gtk.FileChooserNative(_("Select a directory to alter its permessions..."), this, Gtk.FileChooserAction.SELECT_FOLDER, _("Select directory"), _("Cancel"));
			}

			fileChooser.response.connect(fileChooserAction);
			fileChooser.set_modal(true);
			fileChooser.show();
		}

		private void applyButtonAction(){
			string fileToAlter=this.selectLocationEntry.get_text();		// This regular expression could be used to validate this ^\/(?:[\w\s\-]+\/?)*(?:[\w\s\-]+\.[\w]+)?$
			Misc.Validator validator=new Misc.Validator();

			if(fileToAlter==""){
				msgDialog=new App.Widgets.MessageDialog(this, WARNING, OK, _("No file or folder has been selected."));
				return;
			}

			if(!validator.validatePath(fileToAlter)){
				msgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("The path given does not appear to be a valid one.\nTry using the \"Browse\" button."));
				return;
			}

			if(!validator.validateNumbers(ownerPermissionEntry.get_text())){
				msgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("The owner's permission must be set to just one number."));
				return;
			}

			if(!validator.validateNumbers(groupPermissionEntry.get_text())){
				msgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("The groups's permission must be set to just one number."));
				return;
			}

			if(!validator.validateNumbers(otherPermissionEntry.get_text())){
				msgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("The others permission must be set to just one number."));
				return;
			}


			string commandToSend="/usr/bin/chmod ";

			// Gets whatever is written in the extra arguments Gtk.Entry
			string extraArgs=argumentsEntry.get_text();

			// Gets the singular permissions written in their respective Gtk.Entries
			string permissionSelected=ownerPermissionEntry.get_text();
			permissionSelected+=groupPermissionEntry.get_text();
			permissionSelected+=otherPermissionEntry.get_text();

			// Sets the permissions to the command. For example '/usr/bin/chmod 644'
			if(permissionSelected!=""){
				commandToSend+=permissionSelected+" ";
			}

			if(extraArgs!=""){
				if(!validator.validateArguments(extraArgs)){
					msgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("Please, check the syntax you are trying to give as extra arguments.\nSome of it is not correct."));
					return;
				}

				// Adds the extra arguments to the command. For example '/usr/bin/chmos 644 --verbose'
				commandToSend+=extraArgs+" ";
			}

			// Adds the file or folder to the command. For example '/usr/bin/chmos 644 --verbose "/home/user/main.c"'
			commandToSend+="\""+fileToAlter+"\"";

			int exitCode=Posix.system(commandToSend);
			if(exitCode!=0){
				msgDialog=new App.Widgets.MessageDialog(this, ERROR, OK, _("An error occurred while trying to apply the setted permissions to\n")+fileToAlter+_(", maybe the filename is not accessible to the current user."));
			}else{
				generateNotification("org.vinarisoftware.permessi", _("The permissions have applied correctly for ") + fileToAlter +" !");
			}
		}
	}
}
