#!/bin/bash
#
#	Copyright (c) 2015 - 2025, Vinari Software
#	All rights reserved.
#
#	Redistribution and use in source and binary forms, with or without
#	modification, are permitted provided that the following conditions are met:
#
#	1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
#	2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
#	3. Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
#	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
RESET='\033[0m'
APP_NAME="Permessi - Vinari Software"

if ! [ -x "$(command -v valac)" ]; then
	echo -e "\n${RED}ERROR:${RESET} The Vala compiler could not be located...\n"
	exit 1
fi

if ! [ -x "$(command -v gcc)" ]; then
	echo -e "\n${RED}ERROR:${RESET} The C compiler could not be located...\n"
	exit 1
fi

if ! [ -x "$(command -v meson)" ]; then
	echo -e "\n${RED}ERROR:${RESET} The Meson build system could not be located...\n"
	exit 1
fi

if ! [ -x "$(command -v ninja)" ]; then
	echo -e "\n${RED}ERROR:${RESET} The Ninja build tool could not be located...\n"
	exit 1
fi

if ! [ -x "$(command -v gettext)" ]; then
	echo -e "\n${RED}ERROR:${RESET} GNU Gettext could not be located...\n"
	exit 1
fi

echo -e "\n${GREEN}INFO:${RESET} All compilation dependencies checks passed.\nComputer is ready to compile ${APP_NAME}\n\n${YELLOW}WARNING:${RESET} Ensure that you have installed the GTK 4 development files.\n"
exit 0
